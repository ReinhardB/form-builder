$(document).on('click', '#refreshDisplay', function () {
retrieveData();
});

function retrieveData() {
var sortableContainerIds = $('#sortable-layout > .sortable-container-li').map(function () {
return this.id
}).toArray();

sortableContainerLoop(sortableContainerIds);


//Puts all the sortable containers in a array, preset == custom
//var init
var sortableContainerIds = $('#sortable-layout > .sortable-container-li').map(function () {
return this.id
}).toArray();
//We start with our form tag - ############## Still need to add method and action
var fullHtmlAdd = '
<form>';
    sortableContainerLoop(sortableContainerIds, fullHtmlAdd);

    }


    function sortableContainerLoop(sortableContainerIds, fullHtmlAdd) {
    //Run a loop for each of the containers
    $.each(sortableContainerIds, function (i, val) {
    console.log('each Sortable');
    //We get the ID of the a single container `sortable-container-xx`
    var currentID = sortableContainerIds [i];

    //console.log("SortableContainerId each: " + currentID);
    //Lists all the col ids that are in the containers, so if the currently selected container has 4 cols, this will list the 4col ids
    var colIds = $('#' + currentID + ' > .container-col-holder > .container-col').map(function () {
    return this.id
    }).toArray();
    //console.log("Ids of cols in Sortable - Array: " + colIds);
    //We start the display row with a div to contain the items neatly
    //We get the length of the cols array of ids, to see how many cols we have, so we can give the appropriate width to our items and run a loop accordingly
    var totalCols = colIds.length;

    colLoop(colIds, totalCols, fullHtmlAdd);

    });//end loop for each sortable container
    }

    function colLoop(colIds, totalCols, fullHtmlAdd) {
    var colContainerColsHtml = '
    <div class="display-row-container">';
        console.log('Once per sortable');
        for (var n = 0; n < totalCols; ++n) {
        console.log('For totalCols');
        var currentColId = "#" + colIds[n];
        //console.log("Single Col ID (current): " + currentColId);

        var currentColData = currentCol(currentColId, totalCols, fullHtmlAdd, colContainerColsHtml);
        fullHtmlAdd = colContainerColsHtml + currentColData + '
    </div>
    ';
    }//end loop that runs for each col of the current sortable container
    $("#display-output").html(fullHtmlAdd + '
</form>');

}

function currentCol(currentColId, totalCols, fullHtmlAdd, colContainerColsHtml) {

//See if the col is maybe empty, so we just put a empty div with the right width as a placeholder in the row
if (isEmpty($(currentColId))) {
var colContainerColsHtml2 = "
<div class='empty-col-display container-col-" + totalCols + "'>&nbsp;</div>";
console.log('Empty Col');
} else {
//Remember currentColId = "#" + colIds[n]; So for each loop above we will get the next col ID, and then we slice it to give us something like 3-6
var currentColNumber = currentColId.slice(15);
//console.log("Get the number stored in the ID (1-1): " + currentColNumber);
/* Get class from col-data-3-3, element-input element-radio*/
//#col-data-3-6 for example, will have 2 important classes, 1 telling us what type of html element we are working with, and one telling us the sub-type
//#col-data-3-6 class="element-input element-text"
var elementType = ($('#col-data-' + currentColNumber).attr('class').split(' ')[0]).slice(8);
var elementSubType = ($('#col-data-' + currentColNumber).attr('class').split(' ')[1]).slice(8);
//We retrieve the data stored in the #col-data elements, so we
var elementProperties = $(('#col-data-' + currentColNumber)).data('text');
//Just to make sure that we don't display "NaN" in the value field, should this be done with all values??#####################
if (elementProperties['value'] == 'empty') {
var inputVal = '';
} else {
var inputVal = elementProperties['value'];
}
//Here we look at what type of element is found in the #col-data element and we can display it...
if (elementType == 'input') {
colContainerColsHtml2 += "
<input value='" + inputVal + "' class='container-col-" + totalCols + " " + elementProperties[' class'] + "' id='" + elementProperties['id'] + "' placeholder='" + elementProperties['placeholder'] + "' type='" + elementSubType + "'>";
}
//Here we will add more if statements for different elements

console.log('Not empty col');
}//end if currentColID isempty (else)
fullHtmlAdd += colContainerColsHtml + '</div>';
return fullHtmlAdd;
}

function isEmpty(el) {
return !$.trim(el.html())
}


<!DOCTYPE html>
<html lang=en>
<head>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta http-equiv=X-UA-Compatible content="IE=edge,chrome=1">
    <meta name=HandheldFriendly content=true>
    <meta charset=UTF-8>
    <meta name=keywords content="Mast, Tower, Lattice, Pole, Rapid Deployment, Fencing, Streetlight, Self-supporting, Telecommunication, Mobile, Data, Network, Lighting, Guyed, Antenna, Foundation">
    <meta name=description content="We are a major supplier of Masts & Towers in Southern Africa, with over 17 years of experience in manufacturing self-supporting structures to various industries.">
    <title>Modular Masts & Towers - Manufacturers of Lattice Towers & Masts</title>
    <script type="text/javascript">
        //<![CDATA[
        try {
            if (!window.CloudFlare) {
                var CloudFlare = [{
                    verbose: 0,
                    p: 0,
                    byc: 0,
                    owlid: "cf",
                    bag2: 1,
                    mirage2: 0,
                    oracle: 0,
                    paths: {cloudflare: "/cdn-cgi/nexp/dok3v=1613a3a185/"},
                    atok: "b6c02a8d1b3292df07cdafbc8189b7f8",
                    petok: "e1f65d72c5b5e7dec696e7e7862528b1740ba55a-1476862937-86400",
                    zone: "modularmast.co.za",
                    rocket: "a",
                    apps: {}
                }];
                document.write('<script type="text/javascript" src="//ajax.cloudflare.com/cdn-cgi/nexp/dok3v=088620b277/cloudflare.min.js"><' + '\/script>');
            }
        } catch (e) {
        }
        ;
        //]]>
    </script>
    <link rel=stylesheet type=text/css href=css/layout.min.css>
    <link rel=stylesheet href=js/fancybox/jquery.fancybox-1.3.4.min.css type=text/css media=screen>
    <link rel=apple-touch-icon sizes=57x57 href=img/favicon/apple-icon-57x57.png>
    <link rel=apple-touch-icon sizes=60x60 href=img/favicon/apple-icon-60x60.png>
    <link rel=apple-touch-icon sizes=72x72 href=img/favicon/apple-icon-72x72.png>
    <link rel=apple-touch-icon sizes=76x76 href=img/favicon/apple-icon-76x76.png>
    <link rel=icon type=image/png sizes=32x32 href=img/favicon/favicon-32x32.png>
    <link rel=icon type=image/png sizes=96x96 href=img/favicon/favicon-96x96.png>
    <link rel=icon type=image/png sizes=16x16 href=img/favicon/favicon-16x16.png>
    <link rel=manifest href=img/favicon/manifest.json>
    <meta name=msapplication-TileColor content=#ffffff>
    <meta name=theme-color content=#ffffff>
<body>
<script type="text/rocketscript">(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-59452920-5', 'auto'); ga('send', 'pageview');</script>
<div class=content>
    <div class="lockedNav">
        <div class=logo-div>
            <img class=logo src=img/logo.png alt="Modular Masts & Towers - Manufacturers of Lattice Towers & Masts">
        </div>
        <div class=navigation><br>
            <div class=navbuttons><a href=#introA>Home</a> <a href=#aboutA>About</a>
                <a href=#servicesA>Products & Services</a> <a href=#contactA>Contact us</a></div>
        </div>
    </div>
    <div class=contentleft><a id="introA" style="margin-top: -200px;"></a>
        <div id=intro><h1>MODULAR MASTS & TOWERS</h1>
            <p>Welcome to our company which designs & manufactures Masts & Towers.</p>
            <p>We pride ourselves that a large part of the mobile & data networks is literally founded on our structures, when you contact us our masts will be part of the infrastructure used.</p>
            <p>Thousands of our towers weather the elements already since 1998, with an impeccable track record proving the integrity and quality of our products - they last.</p>
            <p>From Rooftop, BTS, Rapid Deployment, Backbone to High Site, Fencing to enclose your sites, we can help, and of course we will design the foundations required.</p>
        </div>
        <div id=services><h1>PRODUCTS & SERVICES</h1>
            <a id="servicesA" style="margin-top: -200px; position: absolute"></a>
            <p>Our core business is self-supporting structures for telecommunication purposes, like Lattice Masts using Angle or Tube, triangular or square. Guyed towers are available too.</p>
            <p>Rapid Deployment sites without permanent concrete foundations, with or without fencing, we have them.</p>
            <p>But also Monopole Towers, Streetlights, Lighting Masts, Camera Poles, various types of Fencing or any ancillary products like Antenna bracketry, if it's steel we make it.</p>
            <br><br><a class=grouped_elements rel=gallery href=img/photos/1.jpg>
                <div id=galleryBtn class=orangebtn>VIEW GALLERY</div>
            </a></div>
    </div>
    <div class=contentright>
        <div id=about><h1>ABOUT US</h1> <a id="aboutA" style="margin-top: -200px; position: absolute"></a>
            <p>From a humble start-up in 1998 we have grown to become a major supplier of Masts and Towers in Southern Africa.<br>With our in-house software we design according to your needs, then manufacture & galvanize all to ISO9001. We deliver and erect the structures on site if so required.
            </p>
            <p>To minimize cost we tailor designs to your needs, just tell us what is required. Short turnaround times allow us to tie in with your project schedule so it's no wonder we supply to all major networks. As we know Africa and its varied conditions like no other we will be an asset for your project on this continent or beyond - let's talk.</p>
        </div>
        <div id=contact><h1>CONTACT US</h1> <a id="contactA" style="margin-top: -200px; position: absolute"></a>
            <p></p>
            <p>Modular Masts and Towers<br><br>Address:<br>Stand 4 First Street<br>Babelegi Gauteng South Africa<br><br>Office number:<br>+27 12 719 8484<br><br>Email Bart on:<br>
            </p>
            <img style=margin-top:-13px;position:absolute src=img/contact/management.png><br><br><br><br><a id=map href="https://www.google.com/maps/embed?pb=!1m13!1m11!1m3!1d1258.6941683844489!2d28.278942664079523!3d-25.369470355781772!2m2!1f0!2f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sen!2sza!4v1455282811972&z=20">
                <div id=mapBtn class=orangebtn>VIEW MAP</div>
            </a></div>
    </div>
</div>
<div class=imgdiv>
    <img class=backgroundimage src=img/background.svg alt="Modular Masts & Towers - Manufacturers of Lattice Towers & Masts">
    <div>
        <img class=backgroundsides src=img/backsides.svg alt="Modular Masts & Towers - Manufacturers of Lattice Towers & Masts">
        <span class=guild_footer>Made with ♥ by <a href=https://guild.co.za style=font-size:10px;color:#6c6c6c;text-decoration:none;visible:none target=_blank><strong>The Creatives Guild</strong></a></span>
    </div>
</div>
<div class=backcolor1></div>
<div class=backcolor2></div>
<div class=backcolor3></div>
<div class=backcolor4></div>
<div class=backcolor5></div>
<script data-rocketsrc=https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js type="text/rocketscript"></script>
<script type="text/rocketscript">$(document).ready(function (){$('a[href^="#"]').on("click", function (n){n.preventDefault(); var t=this.hash, o=$(t); $("html, body").stop().animate({scrollTop: o.offset().top}, 900, "swing", function (){window.location.hash=t})})});</script>
<script type=text/javascript src=js/fancybox/jquery.fancybox-1.3.4.pack.js></script>
<script type="text/rocketscript" data-rocketsrc="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/rocketscript">$(document).ready(function (){$("a.grouped_elements").fancybox({'transitionIn': 'fade', 'transitionOut': 'fade', 'titlePosition': 'over', 'titleFormat': function (title, currentArray, currentIndex, currentOpts){return '<span class="imgTitleSpan" id="fancybox-title-over"><p class="imgTitle" id="image' + currentIndex + '">' + (title.length ? ' &nbsp; ' + title : '') + '</p></span>';}}); $("#map").fancybox({'width': '75%', 'height': '80%', 'autoScale': false, 'transitionIn': 'none', 'transitionOut': 'none', 'type': 'iframe'});});$('#mapBtn').on('click', function (){$('#googleMap').show();});</script>
<span id="fancybox-title-over"></span><a style=display:none href=img/photos/1.jpg><img src=img/photos/1.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/2.jpg><img src=img/photos/2.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/3.jpg><img src=img/photos/3.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/4.jpg><img src=img/photos/4.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/5.jpg><img src=img/photos/5.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/6.jpg><img src=img/photos/6.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/7.jpg><img src=img/photos/7.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/8.jpg><img src=img/photos/8.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/9.jpg><img src=img/photos/9.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/10.jpg><img src=img/photos/10.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/11.jpg><img src=img/photos/11.jpg alt=""></a><a class=grouped_elements style=display:none rel=gallery href=img/photos/12.jpg><img src=img/photos/12.jpg alt=""></a>