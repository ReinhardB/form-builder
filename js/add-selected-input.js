
$(document).on('click', '.container-col-edit', function () {
    var selectedItem = ($(this).find("div").attr('class').split(' ')[1]).slice(8);
    var edit = 1;
    var editId = ($(this).attr('id')).slice(14);
    $("#step-3-set-input-attributes").empty();
    $(document).ready(function () {
        var newID = 'selected-edit-' + editId;
        $(".options-overlay").attr('id', newID);
    });
    addSelectedInput(selectedItem, edit, editId);


});


$(".add-input-holder").click(function () {
    var selectedItem = $(this).attr('id');
    //console.log(selectedItem);
    $("#step-3-set-input-attributes").empty();
    var edit = 0;
    var editId = "";
    addSelectedInput(selectedItem, edit, editId)
});

function addSelectedInput(selectedItem, edit, editId) {
    $(document).ready(function () {
        if (edit == 0){
            selectedItem = selectedItem.slice(11);
        }
        /* HTML ATTRIBUTES */
        /* Containers */
        var htmlStartDiv        = '<div class="half-col-holder">';
        var htmlHalfDiv         = '<div class="half-col-container">';
        var htmlEndDiv          = '</div>';
        var htmlBr              = '</br>';

        var htmlTableStart      = '<table class="step-3-table"><tbody>';
        var htmlTableEnd        = '</tbody></table>';
        var htmlTableRow        = '<tr>';
        var htmlTableRowEnd     = '</tr>';
        var htmlTableData       = '<td>';
        var htmlTableDataEnd    = '</td>';

        /* Backend */

        var htmlShowHeadHtml    = '<h3>HTML Attributes</h3>';
        var htmlSetID           = '<h5>HTML ID</h5>         <input id="user-set-id" type="text" name="id" value="" placeholder="ID linked to this">';
        var htmlSetClass        = '<h5>HTML Class</h5>      <input id="user-set-class" type="text" name="class" value="htmlID" placeholder="Class linked to this">';
        var htmlSetName         = '<h5>Name</h5>            <input id="user-set-name" type="text" name="name" value="htmlClass" placeholder="HTML name linked to this">';

        /* Frontend AND Backend */
        var htmlShowHeadUserRes = '<h3>User Restrictions</h3>';
        var htmlSetReadOnly     = '<h5>ReadOnly</h5>        <input id="user-set-readonly" type="checkbox" name="readonly" value="" placeholder="">';
        var htmlSetRequired     = '<h5>Required</h5>        <input id="user-set-required" type="checkbox" name="required" value="" placeholder="">';
        var htmlSetMaxLength    = '<h5>Max Length</h5>      <input id="user-set-maxLength" type="number" name="maxLength" value="" placeholder="">';


        /* DisplayEnd */
        var htmlShowHeadDisplay = '<h3>Display end options</h3>';
        var htmlSetValue        = '<h5>Value</h5>           <input id="user-set-value" type="text" name="value" value="" placeholder="">';
        var htmlSetPlaceholder  = '<h5>Placeholder</h5>     <input id="user-set-placeholder" type="text" name="placeholder" value="place" placeholder="">';
        var htmlSetColor        = '<h5>Color</h5>           <input id="user-set-color" type="text" name="color" value="#333" placeholder="">';
        var htmlSetBGColor      = '<h5>Background Color</h5>           <input id="user-set-color" type="text" name="color" value="#333" placeholder="">';

        /* CSS */
        var htmlShowHeadCSS = '<h3>CSS specific options</h3>';
        var htmlSetHeight       = '<h5>Height</h5>          <input id="user-set-height" type="text" name="height" value="123" placeholder="">';
        var htmlSetWidth        = '<h5>Width</h5>           <input id="user-set-width" type="text" name="width" value="123" placeholder="">';
        var htmlSetPadding      = '<h5>Padding</h5>          <input id="user-set-height" type="text" name="height" value="123" placeholder="">';
        var htmlSetMargin       = '<h5>Margin</h5>           <input id="user-set-width" type="text" name="width" value="123" placeholder="">';

        //var htmlRegExTest       ='<input type="text" name="country_code" pattern="[A-Za-z0-9]{3}" title="Three letter country code"><input type="submit">';

        var step3HTML;
        switch (selectedItem) {
            case "text":
                var htmlHolderStart = '<div class="settings-holder" id="input-text-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-text" class="submit-data"><span>Submit</span></div>';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "email":
                var htmlHolderStart = '<div class="settings-holder" id="input-email-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-email" class="submit-data">Submit</div> ';
                step3HTML =
                    step3HTML =
                        htmlHolderStart + htmlTableStart +
                        htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                        htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                        htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                        htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                        htmlTableEnd + htmlHolderEnd;
                break;
            case "radio":
                var htmlHolderStart = '<div class="settings-holder" id="input-radio-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-radio" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetRequired +  htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "checkbox":
                var htmlHolderStart = '<div class="settings-holder" id="input-checkbox-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-checkbox" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetRequired +  htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "button":
                var htmlHolderStart = '<div class="settings-holder" id="input-button-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-button" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "number":
                var htmlHolderStart = '<div class="settings-holder" id="input-number-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-number" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "telephone":
                var htmlHolderStart = '<div class="settings-holder" id="input-telephone-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-telephone" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "textarea":
                var htmlHolderStart = '<div class="settings-holder" id="input-textarea-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-textarea-textarea" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "password":
                var htmlHolderStart = '<div class="settings-holder" id="input-password-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-password" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            case "custom":
                var htmlHolderStart = '<div class="settings-holder" id="input-custom-settings"><form>';
                var htmlHolderEnd = '</form></div><div id="submit-input-custom" class="submit-data">Submit</div> ';
                step3HTML =
                    htmlHolderStart + htmlTableStart +
                    htmlTableRow + htmlTableData + htmlShowHeadHtml + htmlSetID + htmlSetClass + htmlSetName + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadUserRes + htmlSetReadOnly + htmlSetRequired + htmlSetMaxLength + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableRow + htmlTableData + htmlShowHeadDisplay + htmlSetValue + htmlSetPlaceholder + htmlSetColor + htmlSetBGColor + htmlTableDataEnd +
                    htmlTableData + htmlShowHeadCSS + htmlSetHeight + htmlSetWidth + htmlSetPadding + htmlSetMargin + htmlTableDataEnd + htmlTableRowEnd +

                    htmlTableEnd + htmlHolderEnd;
                break;
            default:
                break;
        }
        $("#step-3-set-input-attributes").html(step3HTML).show(200);
        $("#step-2-input").hide(200);
        if (edit == 1){
            console.log("here");
            var inputIds = $('.step-3-table > tbody > tr > td  input').map(function(){
                return this.id;
            }).toArray();
            var currentID;
            var elementDataArr = {};
            elementDataArr = $(("#col-data-"+editId)).data("text");
            console.log("Array Fetch: "+"#col-data-"+editId);
            $.each(inputIds , function(i, val) {
                currentID = inputIds [i];
                var currentIdKey = currentID.slice(9);
                $("#" + currentID).val(elementDataArr[currentIdKey]);

                console.log("Array I :"+ currentIdKey);
                console.log("Array Val :"+ elementDataArr[currentIdKey]);
            });
        }


    });
}