$(".add-container").click(function () {
    addContainer($(this).attr('id'));
});
/*
$(".container-col-custom-add").click(function () {
    addContainerCustom($(this).attr('id'));
});*/
$(".container-col-custom-add").click(function () {
    widthContainerCustom($(this).attr('id'));
});
function widthContainerCustom(containerID) {
    var containerCols = containerID.slice(25);
    containerCols = parseInt(containerCols);
    $(".container-add-custom-width").html("");
    for (var n = 0; n < containerCols; ++n) {
        var colId = n+1;
        var currentCol = "<div id='container-col-custom-col-"+colId+"' class='container-custom-width container-col container-col-"+containerCols+"'>" +
            "<form>"+
            "<input class='custom-width-range' type='range' name='amountRange' min='1' max='12' value='1' oninput='this.form.amountInput.value=this.value' />"+
            "<input id='custom-width-input-"+colId+"' type='number' name='amountInput' min='1' max='12' value='1' oninput='this.form.amountRange.value=this.value' readonly />"+
            "</form>"+
            "</div>";
        $(".container-add-custom-width").append(currentCol);
    }

    $(".container-add-custom-width").append("<br><br><br><br><div id='add-custom-widths-"+containerCols+"' class='submit-button add-custom-widths'>ADD</div>");

}
$(document).on('click', '.add-custom-widths', function () {
    var addButtonId = ($(this).attr('id'));
    addButtonId = addButtonId.slice(18);
    addButtonId = parseInt(addButtonId);
    var totalWidth = 0;
    var allWidths = [];
    for (var n = 0; n < addButtonId; ++n){
        totalWidth = totalWidth + parseInt($("#custom-width-input-"+(n+1)).val());
        console.log(totalWidth);
        allWidths[n+1] = parseInt($("#custom-width-input-"+(n+1)).val());
    }
    console.log(allWidths);
    if (totalWidth !== 12){
        alert("Total cols must be under 12. Current = "+totalWidth);
    }else {
        addContainerCustom(addButtonId, allWidths);
    }
});



function addContainerCustom(containerID, allWidths) {
    $(document).ready(function () {
        containerID = parseInt(containerID);
        /* Get lowest available ID */
        var testID = '#sortable-container-1';
        var loopIncId = 1;
        while (($(testID).length) == 1) {
            loopIncId = loopIncId +1;
            testID = '#sortable-container-' + loopIncId;
        }
        var newID = loopIncId;
        /* Add delete and first container-col*/
        var containerInside = '<div id="container-delete-' + newID + '" class="container-delete close-button"></div><div id="container-col-' + newID + '-1" class="container-custom container-custom-'+allWidths[1]+' container-col-empty container-col">' + '</div>';
        /* Loop for 2-6 cols */
        for (i = 1; i < containerID; i++) {
            containerInside += '<div id="container-col-' + newID + '-' + (i + 1) + '" class="container-custom container-custom-'+allWidths[i + 1]+' container-col-empty container-col">' + '</div>';
        }
        /* Add container with selected amount of cols */
        $("#sortable-layout").append('<li id="sortable-container-' + newID + '" class="sortable-container-li"><div class="container-col-holder">' + containerInside + '</div></li>');
        $("#add-layout-container").toggle(200);

    });
}


/*Function to add containers, with appropriate cols, IDs and Classes*/
function addContainer(containerID) {
    $(document).ready(function () {
        containerID = containerID.slice(17);
        containerID = parseInt(containerID);
        /* Get lowest available ID */
        var testID = '#sortable-container-1';
        var loopIncId = 1;
        while (($(testID).length) == 1) {
            loopIncId = loopIncId +1;
            testID = '#sortable-container-' + loopIncId;
        }
        var newID = loopIncId;
        /* Add delete and first container-col*/
        var containerInside = '<div id="container-delete-' + newID + '" class="container-delete close-button"></div><div id="container-col-' + newID + '-1" class="container-col-empty container-col container-col-' + containerID + '">' + '</div>';
        /* Loop for 2-6 cols */
        for (i = 1; i < containerID; i++) {
            containerInside += '<div id="container-col-' + newID + '-' + (i + 1) + '" class="container-col-empty container-col container-col-' + containerID + '">' + '</div>';
        }
        /* Add container with selected amount of cols */
        $("#sortable-layout").append('<li id="sortable-container-' + newID + '" class="sortable-container-li"><div class="container-col-holder">' + containerInside + '</div></li>');
        $("#add-layout-container").toggle(200);

    });
}
/* ADD CONTAINERS-END */

/* DELETE CONTAINERS */
$(document).on('click', '.container-delete', function () {
    var thisID = "sortable-container-" + ($(this).attr('id')).slice(17);
    console.log(thisID);
    $("#"+thisID).remove();
});



/*ADD ITEMS FUNCTIONS*/
/*Goes to step 1*/
$(document).on('click', '.container-col-empty', function () {
    $("#add-step-1").show();
    $(".options-overlay").show(200);
    addColItem($(this).attr('id'));
});
function addColItem(colID) {
    $(document).ready(function () {
        colID = colID.slice(14);
        colID = 'selected-edit-' + colID;
        $(".options-overlay").attr('id', colID);
    });
}
