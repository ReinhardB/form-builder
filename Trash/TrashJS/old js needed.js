/*=======================STEP 1 ADD CUSTOM INPUTS========================*/

/* OPEN INPUT SELECT */
$("#add-element").click(function () {
    $(document).ready(function () {
        $("#customize-attributes").toggle(200);
    });
});

/* SELECT INPUT TYPE */
$("#custom-text").click(function () {
    toggleStep2("#custom-step-2-text");
});
$("#input-text-select").click(function () {
    toggleStep2("#custom-step-2");
});
$("#input-textarea-select").click(function () {
    toggleStep2("#custom-step-2");
});
$("#input-checkbox-select").click(function () {
    toggleStep2("#custom-step-2");
});
$("#input-radio-select").click(function () {
    toggleStep2("#custom-step-2");
});

function toggleStep2(step2ID) {
    $(document).ready(function () {
        $("#custom-step-1").toggle();
        $(step2ID).toggle();
    });
}

/* TEXT INPUT SELECTED, ADD */
$("#add-text-input-button").click(function () {
    $(document).ready(function () {
        //Set input values - label, name, value - to variables
        var inputlabel = $("#step2-label").val();
        var inputname = $("#step2-name").val();
        var inputvalue = $("#step2-value").val();
        //Get Current ID, and set New ID
        var currentID = $('#origin-box div.dragcurrent').attr('id');
        $('#origin-box div.dragcurrent').removeAttr('id');
        currentID = currentID.slice(12);
        var iCurrentID = parseInt(currentID);
        var current = iCurrentID + 1;
        var newCurrentID = 'dragcurrent-' + current;
        $('#origin-box div.dragcurrent').attr('id', newCurrentID);
        //Got all the data needed, append data to HTML
        $("#origin-box").append('<div ondragstart="dragStart(event)" ondrag="dragging(event)" draggable="true" class="dragtarget" id="dragtarget-' + current + '">' + inputname + '</div>');
        //Set name for input and add data to element. Name used user input with element ID number, so is unique
        inputname = inputname + '-' + current;
        $('#dragtarget-' + current).data("label", inputlabel);
        $('#dragtarget-' + current).data("name", inputname);
        $('#dragtarget-' + current).data("value", inputvalue);
    });
});
// Add just text Element
$("#add-plain-text-button").click(function () {
    $(document).ready(function () {
        var textText = $("#step2-text-text").val();
        var textSize = $("#step2-text-size").val();
        var textAlign = $("#step2-text-align").val();
        //Get currentID and set new ID
        var currentID = $('#origin-box div.dragcurrent').attr('id');
        $('#origin-box div.dragcurrent').removeAttr('id');
        currentID = currentID.slice(12);
        var iCurrentID = parseInt(currentID);
        var current = iCurrentID + 1;
        var newCurrentID = 'dragcurrent-' + current;
        //Sets the ID for currentID holder Element;
        $('#origin-box div.dragcurrent').attr('id', newCurrentID);
        //Got all the data needed, append data to HTML
        $("#origin-box").append('<div ondragstart="dragStart(event)" ondrag="dragging(event)" draggable="true" class="dragtarget drag-text" id="dragtarget-' + current + '">' + textText + '<span class="edit-text" id="editTarget-' + current + '">E</span></div>');
        //Set data text options for element
        $('#dragtarget-' + current).data("text", textText);
        $('#dragtarget-' + current).data("size", textSize);
        $('#dragtarget-' + current).data("align", textAlign);
        //Edit Data for Text Element. Does it belong here?
        $(".edit-text").click(function () {
            //Set ID for target Element
            var idVal = $(this).attr('id');
            idVal = idVal.slice(11);
            var editTarget = 'dragtarget-' + idVal;
            //Remove ID of save Button and sets it to
            $(".save-text-changes").removeAttr('id');
            $(".save-text-changes").attr( 'id', editTarget );
            editTarget = '#' + editTarget;
            var curentText = $(editTarget).data('text');
            var curentSize = $(editTarget).data('size');
            var curentAlign = $(editTarget).data('align');
            $("#text-text-edit").val(curentText);
            $("#text-size-edit").val(curentSize);
            $("#text-align-edit").val(curentAlign);
            $(".form-options-holder").toggle(200);
            $(".text-settings").toggle(200);
        });
        //Save text changes button
        $(".save-text-changes").click(function () {
            //Set colors from input
            var backgroundColor = $("#color-output").css("background-color");
            var textColor = $("#color-output").css("color");
            var borderColor = $("#color-output").css("border-color");
            //Get Target
            var idVal = $(this).attr('id');
            idVal = idVal.slice(11);
            var editTarget = '#dragtarget-' + idVal;
            //Set text/size/align for selected target
            var textText = $("#text-text-edit").val();
            var textSize = $("#text-size-edit").val();
            var textAlign = $("#text-align-edit").val();
            //Set Border values
            var borderStyle;
            var borderInput = $("input[name=border-style]:checked").val();
            var borderWidth = $("input[name=border-width]").val();
            borderWidth = borderWidth + 'px'

            function Function() {
                //do stuff
            }

            //Find out what input was selected
            if (borderInput == 0) {
                borderStyle = 'solid';
            } else if(borderInput == 1) {
                borderStyle = 'dashed';
            }else {
                borderStyle = 'none';
            }
            var borderCSS = borderStyle + ' ' + borderWidth + ' ' +  borderColor + ' ';
            //Set new Data, Text, Size, Align
            $(editTarget).data("text", textText);
            $(editTarget).data("size", textSize);
            $(editTarget).data("align", textAlign);
            //Set new Data, Color, Background-Color, Border
            $(editTarget).data("color", textColor);
            $(editTarget).data("background-color", backgroundColor);
            $(editTarget).data("border-css", borderCSS);
            //Hide settings page after save is clicked
            $(".form-options-holder").toggle(200);
            $(".text-settings").toggle(200);

        });
    });
});
/*---------------------------END STEP 1----------------------------------*/




/*=========================DISPLAY CURRENT=======================================*/
$("#refresh-display").click(function () {
    $(document).ready(function () {
        $("#display-form").empty();
        //Get form settings from user inputs
        var displayWidth = parseInt($("#user-form-width").val());
        var displayMargin = parseInt($("#user-form-margin").val());
        var displayPadding = parseInt($("#user-form-padding").val());
        //Set form width
        $("#display-form").css('width', displayWidth);

        //Get ID of last element to display, so get the number of elements
        var currentID = $('#droptarget-container div.droptarget-gui:last').attr('id');
        currentID = currentID.slice(11);
        iCurrentID = parseInt(currentID);
        //For loop for each element to display
        for (var n = 0; n < iCurrentID; ++n) {
            var current = n + 1;
            //Set some defaults
            var dataTarget = '#droptarget-' + current + ' .dragtarget';
            var inputWidth = '99%';
            var addBreak = "";
            //Find how many Cols there is, sets width for each element
            if ($('#droptarget-' + current).hasClass("drop-col-1")) {
                addBreak = "<br>";
            } else if ($('#droptarget-' + current).hasClass("drop-col-2")) {
                inputWidth = "49%";
            } else if ($('#droptarget-' + current).hasClass("drop-col-3")) {
                inputWidth = "32.333%";
            } else if ($('#droptarget-' + current).hasClass("drop-col-4")) {
                inputWidth = "24%";
            } else if ($('#droptarget-' + current).hasClass("drop-col-5")) {
                inputWidth = "19%";
                console.log('Variables Set');
            } else if ($('#droptarget-' + current).hasClass("drop-col-6")) {
                inputWidth = "15.667%";
            }
            //Set color and text variables from element data
            var textSize = $(dataTarget).data('size');
            var textAlign = $(dataTarget).data('align');
            var textText = $(dataTarget).data('text');

            var textColor = $(dataTarget).data('color');
            var backgroundColor = $(dataTarget).data('background-color');
            var borderCSS = $(dataTarget).data('border-css');

            if ($(dataTarget).hasClass("drag-text")) {

                var inputStyle = 'margin-right: 1%; font-size: ' + textSize + 'px;' ;
                //Make sure change works (moved some input style to div style
                var divStyle = 'width: ' + inputWidth + '; float: left; text-align: ' + textAlign + '; color: '+textColor+'; background-color: '+backgroundColor+'; border: '+borderCSS+';';
                $("#display-form").append('<div class="user-css-target-div" style="box-sizing: border-box; ' + divStyle + '"><span style="' + inputStyle + '" type="text" id="display-input-' + namedata + '" placeholder="' + placeholderdata + '" value="' + valuedata + '">' + textText + '</span></div>' + addBreak);
            } else {
                var inputStyle = 'width: ' + inputWidth + '; margin-right: 1%;';
                //var inputStyle = 'margin-right: 1%; font-size: ' + textSize + 'px; color: '+textColor+'; background-color: '+backgroundColor+'; border: '+borderCSS+';' ;
                var valuedata = $(dataTarget).data('value');
                var namedata = $(dataTarget).data('name');
                var placeholderdata = $(dataTarget).data('label');
                $("#display-form").append('<input style="' + inputStyle + '" type="text" id="display-input-' + namedata + '" placeholder="' + placeholderdata + '" value="' + valuedata + '">' + addBreak);
            }
        }
        $("#display-form input, #display-form .user-css-target-div").css('margin-top', displayMargin);
        $("#display-form input, #display-form .user-css-target-div").css('padding', displayPadding);

    });
});

/* ===================== Add drop target ========================== */
$("#add-droptarget").click(function () {
    $(document).ready(function () {
        $("#add-drop-target-select-form").toggle(200);
    });
});


function addCol(totalCols) {
    $(document).ready(function () {
        var appendHTMLData = '';
        var currentID = $('#droptarget-container div.droptarget-gui:last').attr('id');
        currentID = currentID.slice(11);
        iCurrentID = parseInt(currentID);
        newID = iCurrentID + 1;
        for (var n = 0; n < totalCols; ++n) {
            appendHTMLData = appendHTMLData + '<div class="drop-target-col-holder sortable"><div id="droptarget-' + newID + '" class="droptarget droptarget-gui drop-col-'+totalCols+' position-'+totalCols+'" ondrop="drop(event)" ondragover="allowDrop(event)" ondrag=""></div>';
        }
        $("#droptarget-container").append(appendHTMLData);
        $("#add-drop-target-select-form").toggle(200);
    });
}

/* 1COL */
$("#add-col-1").click(function () {
    addCol(1);
});
/* 2COL */
$("#add-col-2").click(function () {
    addCol(2);
});
/* 3COL */
$("#add-col-3").click(function () {
    addCol(3);
});
/* 4COL */
$("#add-col-4").click(function () {
    addCol(4);
});
/* 5COL */
$("#add-col-5").click(function () {
    addCol(5);
});
/* 6COL */
$("#add-col-6").click(function () {
    addCol(6);
});

/*-----------------------END Add drop target--------------------------*/


/*=========================== EDIT ELEMENTS ==================================*/
/*

 /*---------------------------END EDIT ELEMENTS--------------------------------*/


/*=========================== FUNCTIONS ==================================*/
/* PREVENT SPACE INPUT */
$('#step2-name').keyup(function () {
    str = $(this).val();
    str = str.replace(/\s/g, '');
    $(this).val(str);
});

$("#toggle-color-picker").click(function () {
    $(".color-settings").toggle(200);
    $(".form-options-holder").toggle(200);

});

/*---------------------------FUNCTIONS END--------------------------------*/


/*=======================INITIALIZE DEFAULTS============================*/
$('#dragtarget-1').data("label", "Name");
$('#dragtarget-1').data("name", "name");
$('#dragtarget-1').data("value", "");

$('#dragtarget-2').data("label", "Surname");
$('#dragtarget-2').data("name", "surname");
$('#dragtarget-2').data("value", "");

$('#dragtarget-3').data("label", "E-mail");
$('#dragtarget-3').data("name", "email");
$('#dragtarget-3').data("value", "");

$('#dragtarget-4').data("label", "Number");
$('#dragtarget-4').data("name", "number");
$('#dragtarget-4').data("value", "");

/*-----------------------------DEFAULTS END---------------------------------*/


/*==========================TEST FUNCTIONS=================================*/


/*-----------------------------TEST END--------------------------------*/


