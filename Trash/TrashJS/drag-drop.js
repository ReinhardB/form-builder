function dragStart(event) {
    event.dataTransfer.setData("Text", event.target.id);
    var dragItemID = '#' + event.target.id;
    var dragTargetHolder = '#' + $(dragItemID).parent().attr('id');
    $(dragTargetHolder).attr("ondragover", "allowDrop(event)");
}
function allowDrop(event) {
    event.preventDefault();
}

function drop(event) {
    event.preventDefault();
    var dropTargetID = '#' + event.target.id;
    $(dropTargetID).attr("ondragover", "");
    console.log(dropTargetID);
    var data = event.dataTransfer.getData("Text");
    event.target.appendChild(document.getElementById(data));
    document.getElementById("demo").innerHTML = "The p element was dropped";
}