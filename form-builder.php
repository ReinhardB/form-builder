<?php
/**
 * Created by PhpStorm.
 * User: Reinhard
 * Date: 2016/09/21
 * Time: 1:47 PM
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jQuery UI Sortable - Default functionality</title>
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/sortable-boxes.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="js/jquery-ui.js"></script>
</head>
<body>

<div id="click-stop">
</div>

<!--Sortable layout containers-->
<div class="all-container-elements">
    <div class="layout-containers-wrapper">
        <ul id="sortable-layout">
            <!-- <li id="sortable-container-1" class="sortable-container-li">
                 <div>Item 1</div>
             </li>
             <li id="sortable-container-2" class="sortable-container-li">
                 <div></div>
                 Item 2
             </li>
             <li id="sortable-container-3" class="sortable-container-li">
                 <div></div>
                 Item 3
             </li>
             <li id="sortable-container-4" class="sortable-container-li">
                 <div></div>
                 Item 4
             </li>
             <li id="sortable-container-5" class="sortable-container-li">
                 <div></div>
                 Item 5
             </li>
             <li id="sortable-container-6" class="sortable-container-li">
                 <div></div>
                 Item 6
             </li>
             <li id="sortable-container-7" class="sortable-container-li">
                 <div></div>
                 Item 7
             </li>-->
        </ul>
        <br>
        <div id="add-drop-target-button"></div>
    </div>
    <div id="add-layout-container">
        <div id="close-layout-container" class="close-button"></div>
        <br>
        <ul id="add-layout-container-ul">
            <!--<li id="add-container-col1" class="add-container"><span>1 column</span></li>
            <li id="add-container-col2" class="add-container"><span>2 columns</span></li>
            <li id="add-container-col3" class="add-container"><span>3 columns</span></li>
            <li id="add-container-col4" class="add-container"><span>4 columns</span></li>
            <li id="add-container-col5" class="add-container"><span>5 columns</span></li>
            <li id="add-container-col6" class="add-container"><span>6 columns</span></li>-->
            <li id="add-container-col1" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-1-1" class="container-col container-col-no-click container-col-1"></div>
                </div>
            </li>
            <li id="add-container-col2" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-2-1" class="container-col container-col-no-click container-col-2"></div>
                    <div id="container-col-2-2" class="container-col container-col-no-click container-col-2"></div>
                </div>
            </li>
            <li id="add-container-col3" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-3-1" class="container-col container-col-no-click container-col-3"></div>
                    <div id="container-col-3-2" class="container-col container-col-no-click container-col-3"></div>
                    <div id="container-col-3-3" class="container-col container-col-no-click container-col-3"></div>
                </div>
            </li>
            <li id="add-container-col4" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-4-1" class="container-col container-col-no-click container-col-4"></div>
                    <div id="container-col-4-2" class="container-col container-col-no-click container-col-4"></div>
                    <div id="container-col-4-3" class="container-col container-col-no-click container-col-4"></div>
                    <div id="container-col-4-4" class="container-col container-col-no-click container-col-4"></div>
                </div>
            </li>
            <li id="add-container-col5" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-5-1" class="container-col container-col-no-click container-col-5"></div>
                    <div id="container-col-5-2" class="container-col container-col-no-click container-col-5"></div>
                    <div id="container-col-5-3" class="container-col container-col-no-click container-col-5"></div>
                    <div id="container-col-5-4" class="container-col container-col-no-click container-col-5"></div>
                    <div id="container-col-5-5" class="container-col container-col-no-click container-col-5"></div>
                </div>
            </li>
            <li id="add-container-col6" class="add-container">
                <div class="container-col-holder">
                    <div id="container-col-6-1" class="container-col container-col-no-click container-col-6"></div>
                    <div id="container-col-6-2" class="container-col container-col-no-click container-col-6"></div>
                    <div id="container-col-6-3" class="container-col container-col-no-click container-col-6"></div>
                    <div id="container-col-6-4" class="container-col container-col-no-click container-col-6"></div>
                    <div id="container-col-6-5" class="container-col container-col-no-click container-col-6"></div>
                    <div id="container-col-6-6" class="container-col container-col-no-click container-col-6"></div>
                </div>
            </li>
            <li id="add-container-col-custom" class="add-container-custom">
                <div class="container-col-holder">
                    <div id="container-col-custom-col-1" class="container-col-custom-add container-col container-col-6">1 Col</div>
                    <div id="container-col-custom-col-2" class="container-col-custom-add container-col container-col-6">2 Col</div>
                    <div id="container-col-custom-col-3" class="container-col-custom-add container-col container-col-6">3 Col</div>
                    <div id="container-col-custom-col-4" class="container-col-custom-add container-col container-col-6">4 Col</div>
                    <div id="container-col-custom-col-5" class="container-col-custom-add container-col container-col-6">5 Col</div>
                    <div id="container-col-custom-col-6" class="container-col-custom-add container-col container-col-6">6 Col</div>
                </div>
            </li>
            <li id="add-container-col-custom-width" class="add-container-custom">
                <div class="container-col-holder container-add-custom-width">
                </div>
            </li>

        </ul>
    </div>
    <div id="data-store-test"></div>
</div>
<!--Display Form-->
<div id="display-output">

</div>

<!--Options Overlay containers-->
<div class="options-overlay options-overlay-hide">
    <div class="options-container">
        <div class="close-button" id="close-options-overlay"></div>
        <h3 class="step-1-hide">Select element category to add</h3>
        <!--Add Elements-->
        <!--Step 1 == Pick type-->
        <div class="options-overlay-hide step-1-hide" id="add-step-1">
            <div class="select-element-add" id="form-input-add">
                <h4>Inputs</h4>
            </div>
            <div class="select-element-add" id="form-text-add">
                <h4>Text</h4>
            </div>
            <div class="select-element-add" id="form-media-add">
                <h4>Media</h4>
            </div>
        </div>

        <!--Step 2-1 == Pick attributes-->
        <div class="options-overlay-hide" id="step-2-input">
            <h3 class="step-heading">Step 2 - Select INPUT type</h3>
            <form>
                <table class="table-select-add-holder">
                    <tbody>
                    <tr>
                        <td>
                            <div id="select-add-text" class="add-input-holder">
                                <h4>Text Input Field</h4>
                                <input type="text" value="Text Input Field">
                            </div>
                        </td>
                        <td>
                            <div id="select-add-email" class="add-input-holder">
                                <h4>Email</h4>
                                <input type="email" value="Email">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="select-add-radio" class="add-input-holder">
                                <h4>Radio buttons</h4>
                                <input type="radio" value="">
                            </div>
                        </td>
                        <td>
                            <div id="select-add-checkbox" class="add-input-holder">
                                <h4>Checkbox</h4>
                                <input type="checkbox" value="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="select-add-button" class="add-input-holder">
                                <h4>Form Submit</h4>
                                <input type="button" value="Submit">
                            </div>
                        </td>
                        <td>
                            <div id="select-add-number" class="add-input-holder">
                                <h4>Number Only</h4>
                                <input type="number" value="">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="select-add-telephone" class="add-input-holder">
                                <h4>Telephone Number</h4>
                                <input type="tel" value="">
                            </div>
                        </td>
                        <td>
                            <div id="select-add-textarea" class="add-input-holder">
                                <h4 style="margin-bottom: 2px!important;">TextArea</h4>
                                <textarea></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="select-add-password" class="add-input-holder">
                                <h4>Password</h4>
                                <input type="password" value="">
                            </div>
                        </td>
                        <td>
                            <div id="select-add-custom" class="add-input-holder">
                                <h4>Custom</h4>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </form>
        </div>

        <div id="step-3-set-input-attributes" class="options-overlay-hide">
            <div class="settings-holder" id="input-text-settings">
                <form>
                    <span>Height</span><br><input type="text" name="height" value="" placeholder="">
                    <span>Width</span><br><input type="text" name="width" value="" placeholder="">
                    <span>Value</span><br><input type="text" name="value" value="" placeholder="">
                    <span>Placeholder</span><br><input type="text" name="placeholder" value="" placeholder="">
                    <span>Name</span><br><input type="text" name="name" value="" placeholder="">
                    <span>HTML ID</span><br><input type="text" name="id" value="" placeholder="">
                    <span>HTML Class</span><br><input type="text" name="class" value="" placeholder="">
                </form>
            </div>
        </div>


        <div id="step-3-add-input" class="options-overlay-hide">

        </div>

        <?php /*
    <!--Step 2-2 == Pick attributes-->
    <div class="options-overlay-hide" id="step-2-text">
        <h3>step-2-text</h3>
    </div>

    <!--Step 2-3 == Pick attributes-->
    <div class="options-overlay-hide" id="step-2-media">
        <h3>step-2-media</h3>
    </div>

    <!--Step 2-4 == Pick attributes-->
    <div class="options-overlay-hide" id="step-2-4">
    </div>

    <!--Step 2-5 == Pick attributes-->
    <div class="options-overlay-hide" id="step-2-5">
    </div>
 */ ?>
    </div>
</div>
<br><br><br><br><br><br><div id="refreshDisplay">Refresh</div>
</body>

<script src="js/sortable-containers.js"></script>
<script src="js/script.js"></script>
<script src="js/add-containers.js"></script>
<script src="js/add-selected-input.js"></script>
<script src="js/display-form.js"></script>
<script src="js/set-input-data.js"></script>
</html>
