/**
 * Created by Reinhard on 2016/09/21.
 */

/*
 ADD SORTABLE CONTAINER


 */




/*
 *  LINK CONTENT OF COLS BACK TO COL, ADD HTML DATA TO COL DIV FOR OPTIONS TO BE SAVED AND RETRIEVED
 *
 * */
$(".add-container").click(function () {
    addContainer($(this).attr('id'));
});
/*Function to add containers, with appropriate cols, IDs and Classes*/
function addContainer(containerID) {
    $(document).ready(function () {
        containerID = containerID.slice(17);
        containerID = parseInt(containerID);

        var lastID = $("#sortable-layout").children().length;
        lastID = (parseInt(lastID)) + 1;
        console.log(lastID);
        lastID = 1;
        var newID = lastID;
        var newIDUp = lastID;
        var testID = 'sortable-container-' + lastID;
        var thisID;
        console.log(testID);
        var testIdInc =  -1;
        while (($(testID).length) == 1) {
            if (newIDUp < 1){
                testIdInc = 1;
            }
            newIDUp = newIDUp + testIdInc;
            thisID = newIDUp
        }

        /*
         while (($(testID).length) == 1) {
         if ((newIDUp > 0)&&(smallerStop == 0))
         {
         newID = newID - 1;
         smallerStop = 1;
         }
         else
         {
         newIDUp = newIDUp + 1;
         }
         testID = '#sortable-container-' + newIDUp;
         thisID = newID;
         }

         */
        /* Add first container-col */
        var containerInside = '<div id="container-delete-' + newID + '" class="container-delete close-button"></div><div id="container-col-' + newID + '-1" class="container-col container-col-' + containerID + '">' + '</div>';
        /* Loop for 2-6 cols */
        for (i = 1; i < containerID; i++) {
            containerInside += '<div id="container-col-' + newID + '-' + (i + 1) + '" class="container-col container-col-' + containerID + '">' + '</div>';
        }
        /* Add container with selected amount of cols */
        $("#sortable-layout").append('<li id="sortable-container-' + newID + '" class="sortable-container-li"><div class="container-col-holder">' + containerInside + '</div></li>');
        $("#add-layout-container").toggle(200);
    });
}
/* DELETE CONTAINERS */



$(document).on('click', '.container-delete', function () {
    var thisID = "sortable-container-" + ($(this).attr('id')).slice(17);
    console.log(thisID);
    $("#"+thisID).remove();
});

/*ADD ITEMS FUNCTIONS*/
/*Goes to step 1*/
$(document).on('click', '.container-col', function () {
    $("#add-step-1").show();
    $(".options-overlay").show(200);
    addColItem($(this).attr('id'));
});
function addColItem(colID) {
    $(document).ready(function () {
        colID = colID.slice(14);
        var selectedContainer = parseInt(colID.split('-', 1)[0]);
        var selectedCol = parseInt(colID.split('-', 1)[1]);
        colID = 'selected-edit-' + colID;
        $(".options-overlay").attr('id', colID);
    });
}

/*ADD INPUT*/
$("#form-input-add").click(function () {
    $(".step-1-hide").hide(50);
    $("#step-2-input").show(200);
});
/*Step 2 ADD INPUT*/

$(".add-input-holder").click(function () {
    var selectedItem = $(this).attr('id');
    addSelectedInput(selectedItem)
});

function addSelectedInput(selectedItem) {
    $(document).ready(function () {
        selectedItem = selectedItem.slice(11);
        $("#step-3-add-input").empty();

        switch (selectedItem) {
            case "text":
                break;
            case "email":
                break;
            case "radio":
                break;
            case "checkbox":
                break;
            case "button":
                break;
            case "number":
                break;
            case "telephone":
                break;
            case "textarea":
                break;
            case "password":
                break;
            case "custom":
                break;
            default:
                break;
        }


    });
}
/*ADD TEXT*/
$("#form-text-add").click(function () {
    $(".step-1-hide").hide(50);
    $("#step-2-text").show(200);
});


/*ADD MEDIA*/
$("#form-media-add").click(function () {
    $(".step-1-hide").hide(50);
    $("#step-2-media").show(200);
});


/*Close add element Overlay*/
$("#close-options-overlay").click(function () {
    $(".options-overlay-hide").hide(200);
});


/* GET ORDER OF SORTABLE DIVS */
var ids = $('#sortable-layout > li').map(function(){
    return (this.id).slice(19);

}).toArray();

/*STORE DATA INPUT CLICK*/






/*might need to initiate arrData every time with the function*/

/* Retrieve Data */

$.each(ids , function(i, val) {
    alert(ids [i]);
});






function Create2DArray(rows) {
    var arr = [];

    for (var i=0;i<rows;i++) {
        arr[i] = [];
    }

    return arr;
}
var arrData = Create2DArray(100);

arrData[1]['color'] = 'blue';
arrData[1]['type'] = 'text';
arrData[1]['width'] = 'based on col amount';

arrData[2]['color'] = 'red';
arrData[2]['type'] = 'submit';
console.log(arrData[2]['type']);

/*
 var items = [
 [1, 2],
 [3, 4],
 [5, 6]
 ];
 items[0][1] = [
 [7, 8],
 [9, 0],
 [58, 86]
 ];
 items[0][2] = [
 [711, 811],
 [911, 110],
 [5118, 8611]
 ];
 console.log(items[0][1]);
 console.log(items[0][2]);


 var items[0][0] = [
 [231, 2, 332],
 [3, 4],
 [5, 6]
 ];
 console.log(items);
 */
/*
 function Create2DArray(rows) {
 var arr = [];

 for (var i=0;i<rows;i++) {
 arr[i] = [];
 }

 return arr;
 }
 var arr = Create2DArray(100);

 arr[50][2] = 5;
 arr[70][5] = 7454;
 */

/*DISPLAY DATA
 * IMPORTANT!!!!!!!
 * 2 loops
 * 1 for each id of holder
 * 1 for each col in holder
 * loop 1 post 1 line at a time
 * loop 2 adds data to same line, can know the amount of cols before loop starts
 * use 1 array of IDs for the first loop, looping for each element in the array,
 *     that would give us a specific ID pointing to a specific element in a array stored in html data
 *
 * maybe store data as:
 *   array["2"=>"array[0=>'type', 1=>'placeholder', 2=>'color', 3=>'anotherValue']"]
 *   so arrayInHtml[2] = [0=>'type', 1=>'placeholder', 2=>'color', 3=>'anotherValue']???
 *
 * */

$('#data-store-test').data("text", ids);
var curentText = $('#data-store-test').data('text');
console.log(curentText);
/*
 var ids = $('#sortable-layout > li').map(function(){
 return this.id
 }).toArray();


 console.log(ids);*/

/*

 var IDs = $("#sortable-layout li[id]").map(function() { return this.id; }).get();


 console.log(IDs);

 */

/*
 //Set data text options for element
 $('#dragtarget-' + current).data("text", textText);
 $('#dragtarget-' + current).data("size", textSize);
 $('#dragtarget-' + current).data("align", textAlign);
 //Edit Data for Text Element. Does it belong here?
 $(".edit-text").click(function () {
 //Set ID for target Element
 var idVal = $(this).attr('id');
 idVal = idVal.slice(11);
 var editTarget = 'dragtarget-' + idVal;
 //Remove ID of save Button and sets it to
 $(".save-text-changes").removeAttr('id');
 $(".save-text-changes").attr( 'id', editTarget );
 editTarget = '#' + editTarget;
 var curentText = $(editTarget).data('text');



 */